# Live backup of KVM virtual machines

This script will let you make backups of live VMs running on KVM, using libvirt.

The backup job will suspend the VM for the time it takes to copy the RAM to disk.

Credits: [Luca Lazzeroni](http://soliton74.blogspot.no/2013/08/about-kvm-qcow2-live-backup.html)

I've made some minor adjustments.